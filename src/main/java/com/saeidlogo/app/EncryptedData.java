/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.app;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author saeidlogo
 */
@Setter
@Getter
public class EncryptedData extends InputData {

    private String name;
    private String phone;
}
