package com.saeidlogo.interceptor;

import com.google.gson.Gson;
import com.saeidlogo.app.InputData;
import com.saeidlogo.annotaions.Encrypted;
import com.saeidlogo.utils.RsaKeyProducer;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.PrivateKey;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

@Provider
@Encrypted
@Priority(Priorities.ENTITY_CODER)
public class DecryptorInterceptorFilter implements ContainerRequestFilter {

    Logger logger = Logger.getLogger(DecryptorInterceptorFilter.class);

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream in = requestContext.getEntityStream();
        // Extract json formatted entity from inputstream request
        String entityJson = IOUtils.toString(in, "UTF-8");
        Gson gson = new Gson();
        InputData msg = gson.fromJson(entityJson, InputData.class);
        // Try to decode json entity with private key and then write it to output stream response
        try {
            String payload = decryptMessage(msg.getMessage());
            out.write(payload.getBytes(Charset.forName("UTF-8")));
        } catch (Exception e) {
            logger.error(e);
            throw new IOException("Unable to decrypt input request");
        }

        byte[] requestEntity = out.toByteArray();
        InputStream is = new ByteArrayInputStream(requestEntity);
        requestContext.setEntityStream(is);
    }

    // Message decoder by private key of JWT
    public String decryptMessage(String message) throws Exception {
        PrivateKey pk = RsaKeyProducer.produce().getPrivateKey();
        return RsaKeyProducer.decrypt(message, pk);
    }
}
