/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.interceptor;

import com.google.gson.Gson;
import com.saeidlogo.app.InputData;
import com.saeidlogo.annotaions.Encrypted;
import com.saeidlogo.utils.RsaKeyProducer;
import java.io.IOException;
import java.security.PublicKey;
import javax.servlet.ServletContext;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import org.apache.log4j.Logger;

/**
 *
 * @author saeidlogo
 */
@Provider
@Encrypted
public class EncryptorInterceptor implements ContainerResponseFilter {

    Logger logger = Logger.getLogger(EncryptorInterceptor.class);

    @Context
    private ServletContext context;

    @Override
    public void filter(ContainerRequestContext request, ContainerResponseContext response) throws IOException {
        PublicKey pubKs = (PublicKey) context.getAttribute(RsaKeyProducer.PUBLIC_KEY);
        if (pubKs != null) {
            Object entity = response.getEntity();
            if (entity != null) {
                Gson gson = new Gson();
                InputData msg = new InputData();
                String value = gson.toJson(entity);
                try {
                    // try to encrypt incomming message with 
                    String cipherTxt = RsaKeyProducer.encrypt(value, pubKs);
                    msg.setMessage(cipherTxt);
                    response.setEntity(msg);
                } catch (Exception e) {
                    logger.error(e);
                    throw new IOException("Unable to cipher txt");
                }
            }
        } else {
            logger.error("public key is not correct");
            throw new IOException("public key is not correct, try to exchange correct public key with server");
        }
    }
}
