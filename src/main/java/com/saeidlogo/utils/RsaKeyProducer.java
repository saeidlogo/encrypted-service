package com.saeidlogo.utils;

import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.Cipher;
import org.apache.log4j.Logger;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.lang.JoseException;
import static org.jose4j.lang.StringUtil.UTF_8;

/**
 * singleton. not using CDI bean on purpose (RsaJsonWebKey is not compatible.
 * this can be worked around though)
 *
 * @author saeidlogo
 */
public class RsaKeyProducer {

    final static Logger logger = Logger.getLogger(RsaKeyProducer.class);

    public static final String PUBLIC_KEY = "publicKey";

    private RsaKeyProducer() {
    }

    private static RsaJsonWebKey theOne;

    /**
     *
     * not an ideal implementation since does not implement double-lock
     * synchronization check
     *
     * @return
     */
    public static RsaJsonWebKey produce() {
        if (theOne == null) {
            try {
                theOne = RsaJwkGenerator.generateJwk(2048);
            } catch (JoseException ex) {
                logger.error(ex);
            }
        }

        return theOne;
    }

    public static String encrypt(String plainText, PublicKey publicKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] cipherText = cipher.doFinal(plainText.getBytes(UTF_8));
        return Base64.getEncoder().encodeToString(cipherText);
    }

    public static String decrypt(String cipherText, PrivateKey privateKey) throws Exception {
        byte[] bytes = Base64.getDecoder().decode(cipherText);
        Cipher decriptCipher = Cipher.getInstance("RSA");
        decriptCipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(decriptCipher.doFinal(bytes), UTF_8);
    }

    public static PublicKey loadPublicKey(String key) {
        try {
            byte[] byteKey = Base64.getDecoder().decode(key);
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");

            return kf.generatePublic(X509publicKey);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            logger.error(e);
        }

        return null;
    }

    public static PrivateKey loadPrivateKey(String key64) throws GeneralSecurityException {
        byte[] clear = Base64.getDecoder().decode(key64);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(clear);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PrivateKey priv = fact.generatePrivate(keySpec);
        Arrays.fill(clear, (byte) 0);
        return priv;
    }

    public static String publishPublicKey() {
        RsaJsonWebKey rsaJsonWebKey = RsaKeyProducer.produce();
        byte[] pk = rsaJsonWebKey.getPublicKey().getEncoded();
        return getKeyAsString(pk);
    }

    public static String getKeyAsString(byte[] bytes) {
        String pkStr = Base64.getEncoder().encodeToString(bytes);
        return pkStr;
    }

    public static PrivateKey getJWTPrivateKey() {
        RsaJsonWebKey rsaJsonWebKey = RsaKeyProducer.produce();
        return rsaJsonWebKey.getPrivateKey();
    }

    public static RsaJsonWebKey generateKeys() throws JoseException {
        return RsaJwkGenerator.generateJwk(2048);
    }
}
