/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.services;

import com.google.gson.Gson;
import com.saeidlogo.app.EncryptedData;
import com.saeidlogo.app.PublicKeyResult;
import com.saeidlogo.app.ServiceResult;
import com.saeidlogo.utils.RsaKeyProducer;
import com.saeidlogo.annotaions.CacheControl;
import com.saeidlogo.annotaions.Encrypted;
import com.saeidlogo.app.EncryptedResult;
import com.saeidlogo.app.InputData;
import com.saeidlogo.app.KeyResult;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.jose4j.jwk.RsaJsonWebKey;

/**
 *
 * @author saeidlogo
 */
@Path("/")
public class SecuredService {

    final Logger logger = Logger.getLogger(SecuredService.class);

    @Context
    private ServletContext context;

    //use this api before calling other encrypted apis to exchage public keys between client and servers
    // use this api only after authentication apis
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/generate/keys")
    @CacheControl("no-cache")
    public Response requestKeys() {
        KeyResult result = new KeyResult();
        try {
            RsaJsonWebKey rsaKeys = RsaKeyProducer.generateKeys();
            String publicKey = RsaKeyProducer.getKeyAsString(rsaKeys.getPublicKey().getEncoded());
            String privateKey = RsaKeyProducer.getKeyAsString(rsaKeys.getPrivateKey().getEncoded());
            result.setPrivateKey(privateKey);
            result.setPublicKey(publicKey);
            result.setStatus(ServiceResult.STATUS_OK);
        } catch (Exception e) {
            logger.error(e);
            result.setErrorMsg(e.getMessage());
        }
        result.setStatus(ServiceResult.STATUS_NOK);
        return Response.ok(result).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/publickey")
    @CacheControl("no-cache")
    //use this api method to get client public key and send it back a public key for server to encrypt messgaes before sending to encrypted requered Apis
    public Response publicKeyExchange(InputData inputData) {
        PublicKeyResult result = new PublicKeyResult();
        String serverPublicKey = RsaKeyProducer.publishPublicKey();
        PublicKey pk = RsaKeyProducer.loadPublicKey(inputData.getMessage());
        context.setAttribute(RsaKeyProducer.PUBLIC_KEY, pk);
        result.setPublicKey(serverPublicKey);
        result.setStatus(ServiceResult.STATUS_OK);
        return Response.ok(result).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/encrypt/message")
    @CacheControl("no-cache")
    // Use this api method to encrypt message before post data to server
    public Response encryptMessage(EncryptedData inputData) {
        ServiceResult result = new ServiceResult();
        String serverPublicKey = RsaKeyProducer.publishPublicKey();
        PublicKey pk = RsaKeyProducer.loadPublicKey(serverPublicKey);
        try {
            Gson gson = new Gson();
            String jsonData = gson.toJson(inputData);
            String msg = RsaKeyProducer.encrypt(jsonData, pk);
            result.setMessage(msg);
            result.setStatus(ServiceResult.STATUS_OK);
        } catch (Exception e) {
            logger.error(e);
            result.setErrorMsg(e.getMessage());
        }
        result.setStatus(ServiceResult.STATUS_NOK);
        return Response.ok(result).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/decrypt/message")
    @CacheControl("no-cache")
    // Use this api method to decrypt enctypted message from server , private key should be provided in test mode
    public Response decryptMessage(InputData inputData) {
        ServiceResult result = new ServiceResult();
        try {
            PrivateKey privateKey = RsaKeyProducer.loadPrivateKey(inputData.getPrivateKey());
            String msg = RsaKeyProducer.decrypt(inputData.getMessage(), privateKey);
            result.setMessage(msg);
            result.setStatus(ServiceResult.STATUS_OK);
        } catch (Exception e) {
            logger.error(e);
            result.setErrorMsg(e.getMessage());
        }
        result.setStatus(ServiceResult.STATUS_NOK);
        return Response.ok(result).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Encrypted
    @Path("/encrypted")
    @CacheControl("no-cache")
    // Call this api method to send an enctypted message to server and after processing receive an encrypted message with client public key 
    public Response encryptedInputApi(EncryptedData data) {
        EncryptedResult result = new EncryptedResult();
        String name = data.getName();
        String phone = data.getPhone();
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String time = dtf.format(now);
        result.setUserInfo("User " + name + " with phone number " + phone + " called api in " + time);
        result.setStatus(ServiceResult.STATUS_OK);
        return Response.ok(result).build();
    }
}
