/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saeidlogo.services;

import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author saeidlogo
 */
@ApplicationPath("/api/v1")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.saeidlogo.interceptor.CacheControlFilter.class);
        resources.add(com.saeidlogo.interceptor.DecryptorInterceptorFilter.class);
        resources.add(com.saeidlogo.interceptor.EncryptorInterceptor.class);
        resources.add(com.saeidlogo.services.SecuredService.class);
    }

}
